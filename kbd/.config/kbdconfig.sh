#!/usr/bin/env bash

setxkbmap -option ctrl:nocaps
xcape -e 'Control_L=Escape' -t 175

echo Now capslock will work as ctrl when used in a combination of keys, and it will work as esc when clicked alone
